#!/bin/bash

# Move to the project directory
docker build -t demo:latest /Users/shivani.boggavarapu/Desktop/springBoot/catalog_management/

cd Users/shivani.boggavarapu/Desktop/springBoot/catalog_management/

# Run Maven clean and package
mvn clean package

# Build the Docker image
docker build -t demo:latest .

# Run Docker Compose
docker-compose up

docker-compose down -v

FROM openjdk:21-jdk

WORKDIR /app

COPY target/catalog_management-0.0.1-SNAPSHOT.jar /app

EXPOSE 8080

CMD ["java", "-jar", "catalog_management-0.0.1-SNAPSHOT.jar"]
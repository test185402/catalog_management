import com.springdemo.catalog_management.Exception.SubcategoryNotFoundException
import com.springdemo.catalog_management.Model.Categories
import com.springdemo.catalog_management.Model.SubCategories
import com.springdemo.catalog_management.Repository.SubCategoriesRepository
import com.springdemo.catalog_management.Service.SubcategoryServiceImpl
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.assertThrows
import org.junit.jupiter.api.extension.ExtendWith
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.Mockito.*
import org.mockito.junit.jupiter.MockitoExtension
import java.util.*

@ExtendWith(MockitoExtension::class)
class SubcategoryServiceImplTest {

    @Mock
    private lateinit var subCategoriesRepository: SubCategoriesRepository

    @InjectMocks
    private lateinit var subCategoryService: SubcategoryServiceImpl

    private lateinit var subCategory: SubCategories
    private lateinit var category: Categories

    @BeforeEach
    fun setUp() {
        category = Categories(1, "Electronics", "Electronic devices")
        subCategory = SubCategories(1, "Subcategory1", category)
    }

    @Test
    fun testGetAllSubCategories() {
        val subCategoriesList = listOf(subCategory)
        `when`(subCategoriesRepository.findAll()).thenReturn(subCategoriesList)

        val result = subCategoryService.getAllSubCategories()

        assertEquals(subCategoriesList, result)
    }

    @Test
    fun testGetSubCategoryById() {
        val subCategoryId = 1
        `when`(subCategoriesRepository.findById(subCategoryId)).thenReturn(Optional.of(subCategory))

        val result = subCategoryService.getSubCategoryById(subCategoryId)

        assertEquals(subCategory, result)
    }

    @Test
    fun testGetSubCategoryByIdNotFound() {
        val subCategoryId = 99
        `when`(subCategoriesRepository.findById(subCategoryId)).thenReturn(Optional.empty())

        val result = subCategoryService.getSubCategoryById(subCategoryId)

        assertNull(result)
    }

    @Test
    fun testCreateSubCategory() {
        `when`(subCategoriesRepository.save(any())).thenReturn(subCategory)

        val result = subCategoryService.createSubCategory(subCategory)

        assertEquals(subCategory, result)
    }

    @Test
    fun testUpdateSubCategory() {
        val updatedSubCategory = SubCategories(1, "Updated Subcategory", category)
        `when`(subCategoriesRepository.findById(1)).thenReturn(Optional.of(subCategory))
        `when`(subCategoriesRepository.save(any())).thenReturn(updatedSubCategory)

        val result = subCategoryService.updateSubCategory(1, updatedSubCategory)

        assertEquals(updatedSubCategory, result)
    }

    @Test
    fun testUpdateSubCategoryNotFound() {
        val updatedSubCategory = SubCategories(99, "Updated Subcategory", category)
        `when`(subCategoriesRepository.findById(99)).thenReturn(Optional.empty())

        val result = subCategoryService.updateSubCategory(99, updatedSubCategory)

        assertNull(result)
    }

    @Test
    fun testDeleteSubCategory() {
        `when`(subCategoriesRepository.existsById(1)).thenReturn(true)

        subCategoryService.deleteSubCategory(1)

        verify(subCategoriesRepository).deleteById(1)
    }

    @Test
    fun testDeleteSubCategoryNotFound() {
        `when`(subCategoriesRepository.existsById(99)).thenReturn(false)

        assertThrows<SubcategoryNotFoundException> { subCategoryService.deleteSubCategory(99) }

        verify(subCategoriesRepository, never()).deleteById(99)
    }
}

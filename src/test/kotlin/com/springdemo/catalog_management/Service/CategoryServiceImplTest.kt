package com.springdemo.catalog_management.Service

import com.springdemo.catalog_management.Exception.CategoryNotFoundException
import com.springdemo.catalog_management.Model.Categories
import com.springdemo.catalog_management.Repository.CategoriesRepository
import org.mockito.ArgumentMatchers.any
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.assertThrows
import org.junit.jupiter.api.extension.ExtendWith
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.Mockito.*
import org.mockito.junit.jupiter.MockitoExtension
import java.util.*

@ExtendWith(MockitoExtension::class)
class CategoryServiceImplTest {

    @Mock
    private lateinit var categoriesRepository: CategoriesRepository

    @InjectMocks
    private lateinit var categoryService: CategoryServiceImpl

    private lateinit var category: Categories

    @BeforeEach
    fun setUp() {
        category = Categories(1, "Electronics", "Electronic devices")
    }

//    This test ensures that the getAllCategories method of the service returns the expected list of categories.

    @Test
    fun testGetAllCategories() {
        val categoriesList = listOf(category)
        `when`(categoriesRepository.findAll()).thenReturn(categoriesList)

        val result = categoryService.getAllCategories()
        assertEquals(categoriesList, result)
    }

    @Test
    fun testGetCategoryById() {
        val categoryId = 1
        `when`(categoriesRepository.findById(categoryId)).thenReturn(Optional.of(category))

        val result = categoryService.getCategoryById(categoryId)

        assertEquals(category, result)
    }

    @Test
    fun testGetCategoryByIdNotFound() {
        val categoryId = 99
        `when`(categoriesRepository.findById(categoryId)).thenReturn(Optional.empty())

        val result = categoryService.getCategoryById(categoryId)

        assertNull(result)
    }

    @Test
    fun testCreateCategory() {
        `when`(categoriesRepository.save(any())).thenReturn(category)

        val result = categoryService.createCategory(category)

        assertEquals(category, result)
    }

    @Test
    fun testUpdateCategory() {
        val updatedCategory = Categories(1, "Updated Electronics", "Updated Electronic devices")
        `when`(categoriesRepository.findById(1)).thenReturn(Optional.of(category))
        `when`(categoriesRepository.save(any())).thenReturn(updatedCategory)

        val result = categoryService.updateCategory(1, updatedCategory)

        assertEquals(updatedCategory, result)
    }

    @Test
    fun testUpdateCategoryNotFound() {
        val updatedCategory = Categories(99, "Updated Electronics", "Updated Electronic devices")
        `when`(categoriesRepository.findById(99)).thenReturn(Optional.empty())

        val result = categoryService.updateCategory(99, updatedCategory)

        assertNull(result)
    }

    @Test
    fun testDeleteCategory() {
        `when`(categoriesRepository.existsById(1)).thenReturn(true)

        categoryService.deleteCategory(1)

        verify(categoriesRepository).deleteById(1)
    }

    @Test
    fun testDeleteCategoryNotFound() {
        `when`(categoriesRepository.existsById(99)).thenReturn(false)

        assertThrows<CategoryNotFoundException> {categoryService.deleteCategory(99)}

        verify(categoriesRepository, never()).deleteById(99)
    }
}
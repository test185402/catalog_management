//package com.springdemo.catalog_management.Service
//
//import com.springdemo.catalog_management.Exception.VariantNotFoundException
//import com.springdemo.catalog_management.Model.Categories
//import com.springdemo.catalog_management.Model.Product
//import com.springdemo.catalog_management.Model.SubCategories
//import com.springdemo.catalog_management.Model.Variant
//import com.springdemo.catalog_management.Repository.VariantRepository
//import org.junit.jupiter.api.Test
//import org.junit.jupiter.api.Assertions.*
//import org.junit.jupiter.api.BeforeEach
//import org.junit.jupiter.api.assertThrows
//import org.junit.jupiter.api.extension.ExtendWith
//import org.mockito.InjectMocks
//import org.mockito.Mock
//import org.mockito.Mockito.*
//import org.mockito.junit.jupiter.MockitoExtension
//import java.util.*
//
//@ExtendWith(MockitoExtension::class)
//class VariantServiceImplTest {
//
//    @Mock
//    private lateinit var variantRepository: VariantRepository
//
//    @Mock
//    private lateinit var productService: ProductService
//
//    @InjectMocks
//    private lateinit var variantService: VariantServiceImpl
//
//    private lateinit var variant: Variant
//    private lateinit var product: Product
//    private lateinit var subCategory: SubCategories
//    private lateinit var category: Categories
//
//    @BeforeEach
//    fun setUp() {
//        category = Categories(1, "Electronics", "Electronic devices")
//        subCategory = SubCategories(1, "Subcategory1", category)
//        product = Product(1, "Laptop", "Powerful laptop with high-performance specs", 4.5, 3, "BrandX", "Model123", 999, subCategory)
//        variant = Variant(1, "Black", 999.0, 50.0, 1.5, "High-resolution display", "15 x 10 x 1.5", product)
//    }
//
//    @Test
//    fun testGetAllVariants() {
//        val variantList = listOf(variant)
//        `when`(variantRepository.findAll()).thenReturn(variantList)
//
//        val result = variantService.getVariants()
//
//        assertEquals(variantList, result)
//    }
//
//    @Test
//    fun testGetVariantById() {
//        val variantId = 1
//        `when`(variantRepository.findById(variantId)).thenReturn(Optional.of(variant))
//
//        val result = variantService.getVariantById(variantId)
//
//        assertEquals(variant, result)
//    }
//
//    @Test
//    fun testGetVariantByIdNotFound() {
//        val variantId = 99
//        `when`(variantRepository.findById(variantId)).thenReturn(Optional.empty())
//
//        val result = variantService.getVariantById(variantId)
//
//        assertNull(result)
//    }
//
//    @Test
//    fun testCreateVariant() {
//        `when`(variantRepository.save(any())).thenReturn(variant)
//
//        val result = variantService.createVariant(variant)
//
//        assertEquals(variant, result)
//    }
//
//    @Test
//    fun testUpdateVariant() {
//        val updatedVariant = Variant(1, "White", 1099.0, 60.0, 1.7, "Improved features", "16 x 11 x 1.7", product)
//        `when`(variantRepository.findById(1)).thenReturn(Optional.of(variant))
//        `when`(variantRepository.save(any())).thenReturn(updatedVariant)
//
//        val result = variantService.updateVariant(1, updatedVariant)
//
//        assertEquals(updatedVariant, result)
//    }
//
//    @Test
//    fun testUpdateVariantNotFound() {
//        val updatedVariant = Variant(99, "White", 1099.0, 60.0, 1.7, "Improved features", "16 x 11 x 1.7", product)
//        `when`(variantRepository.findById(99)).thenReturn(Optional.empty())
//
//        val result = variantService.updateVariant(99, updatedVariant)
//
//        assertNull(result)
//    }
//
//    @Test
//    fun testDeleteVariant() {
//        `when`(variantRepository.existsById(1)).thenReturn(true)
//
//        variantService.deleteVariant(1)
//
//        verify(variantRepository).deleteById(1)
//    }
//
//    @Test
//    fun testDeleteVariantNotFound() {
//        `when`(variantRepository.existsById(99)).thenReturn(false)
//
//        assertThrows<VariantNotFoundException> { variantService.deleteVariant(99) }
//
//        verify(variantRepository, never()).deleteById(99)
//    }
//}

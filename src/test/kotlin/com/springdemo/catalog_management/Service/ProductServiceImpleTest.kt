package com.springdemo.catalog_management.Service
import com.springdemo.catalog_management.Exception.ProductNotFoundException
import com.springdemo.catalog_management.Model.Categories
import com.springdemo.catalog_management.Model.Product
import com.springdemo.catalog_management.Model.SubCategories
import com.springdemo.catalog_management.Repository.ProductRepository
import com.springdemo.catalog_management.Service.ProductServiceImple
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.assertThrows
import org.junit.jupiter.api.extension.ExtendWith
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.Mockito.*
import org.mockito.junit.jupiter.MockitoExtension
import java.util.*

@ExtendWith(MockitoExtension::class)
class ProductServiceImpleTest {

    @Mock
    private lateinit var productRepository: ProductRepository

    @InjectMocks
    private lateinit var productService: ProductServiceImple

    private lateinit var product: Product
    private lateinit var subCategory: SubCategories
    private lateinit var category: Categories

    @BeforeEach
    fun setUp() {
        category = Categories(1, "Electronics", "Electronic devices")
        subCategory = SubCategories(1, "Subcategory1", category)
        product = Product(1, "Laptop", "Powerful laptop with high-performance specs", 4.5, 3, "BrandX", "Model123", 999, subCategory)
    }

    @Test
    fun testGetAllProducts() {
        val productList = listOf(product)
        `when`(productRepository.findAll()).thenReturn(productList)

        val result = productService.getAllProducts()

        assertEquals(productList, result)
    }

    @Test
    fun testGetProductById() {
        val productId = 1
        `when`(productRepository.findById(productId)).thenReturn(Optional.of(product))

        val result = productService.getProductById(productId)

        assertEquals(product, result)
    }

    @Test
    fun testGetProductByIdNotFound() {
        val productId = 99
        `when`(productRepository.findById(productId)).thenReturn(Optional.empty())

        val result = productService.getProductById(productId)

        assertNull(result)
    }

    @Test
    fun testCreateProduct() {
        `when`(productRepository.save(any())).thenReturn(product)

        val result = productService.createProduct(product)

        assertEquals(product, result)
    }

    @Test
    fun testUpdateProduct() {
        val updatedProduct = Product(1, "Updated Laptop", "High-performance laptop with new features", 4.7, 4, "BrandY", "Model456", 1299, subCategory)
        `when`(productRepository.findById(1)).thenReturn(Optional.of(product))
        `when`(productRepository.save(any())).thenReturn(updatedProduct)

        val result = productService.updateProduct(1, updatedProduct)

        assertEquals(updatedProduct, result)
    }

    @Test
    fun testUpdateProductNotFound() {
        val updatedProduct =  Product(99, "Updated Laptop", "High-performance laptop with new features", 4.7, 4, "BrandY", "Model456", 1299, subCategory)
        `when`(productRepository.findById(99)).thenReturn(Optional.empty())

        val result = productService.updateProduct(99, updatedProduct)

        assertNull(result)
    }

    @Test
    fun testDeleteProduct() {
        `when`(productRepository.existsById(1)).thenReturn(true)

        productService.deleteProduct(1)

        verify(productRepository).deleteById(1)
    }

    @Test
    fun testDeleteProductNotFound() {
        `when`(productRepository.existsById(99)).thenReturn(false)

        assertThrows<ProductNotFoundException> { productService.deleteProduct(99) }

        verify(productRepository, never()).deleteById(99)
    }
}
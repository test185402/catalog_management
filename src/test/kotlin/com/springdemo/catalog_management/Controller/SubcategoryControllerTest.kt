package com.springdemo.catalog_management.Controller

import com.fasterxml.jackson.databind.ObjectMapper
import com.springdemo.catalog_management.Model.Categories
import com.springdemo.catalog_management.Model.SubCategories
import com.springdemo.catalog_management.Service.SubcategoryServiceImpl
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.Mockito.*
import org.mockito.junit.jupiter.MockitoExtension
import org.springframework.http.MediaType
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*
import org.springframework.test.web.servlet.result.MockMvcResultHandlers.print
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.status
import org.springframework.test.web.servlet.setup.MockMvcBuilders
import org.junit.jupiter.api.extension.ExtendWith

@ExtendWith(MockitoExtension::class)
class SubcategoryControllerTest  {

    private lateinit var mockMvc: MockMvc

    @Mock
    private lateinit var subcategoryServiceImpl: SubcategoryServiceImpl

    @InjectMocks
    private lateinit var subcategoryController: SubcategoryController

    private lateinit var subcategoryOne: SubCategories
    private lateinit var subcategoryTwo: SubCategories
    private lateinit var categoryOne: Categories
    private lateinit var categoryTwo: Categories

    @BeforeEach
    fun setUp() {
        mockMvc = MockMvcBuilders.standaloneSetup(subcategoryController).build()

        categoryOne = Categories(1, "Electronics", "Electronic devices")
        categoryTwo = Categories(2, "Clothing", "Fashion items")
        subcategoryOne = SubCategories(1, "SubcategoryOne",categoryOne)
        subcategoryTwo = SubCategories(2, "SubcategoryTwo")
    }

    @Test
    fun testGetAllSubCategories() {
        val subcategoriesList = listOf(subcategoryOne, subcategoryTwo)
        `when`(subcategoryServiceImpl.getAllSubCategories()).thenReturn(subcategoriesList)

        mockMvc.perform(get("/catalog/categories/subcategories"))
            .andDo(print())
            .andExpect(status().isOk())
    }

    @Test
    fun testGetSubCategoryById() {
        val subCategoryId = 1
        `when`(subcategoryServiceImpl.getSubCategoryById(subCategoryId)).thenReturn(subcategoryOne)

        mockMvc.perform(get("/catalog/categories/subcategories/$subCategoryId"))
            .andDo(print())
            .andExpect(status().isOk())
    }

    @Test
    fun testGetSubCategoryByIdNotFound() {
        val subCategoryId = 99
        `when`(subcategoryServiceImpl.getSubCategoryById(subCategoryId)).thenReturn(null)

        mockMvc.perform(get("/catalog/categories/subcategories/$subCategoryId"))
            .andDo(print())
            .andExpect(status().isNotFound())
    }

    @Test
    fun testCreateSubCategory() {
        `when`(subcategoryServiceImpl.createSubCategory(subcategoryOne)).thenReturn(subcategoryOne)

        mockMvc.perform(
            post("/catalog/categories/subcategories")
                .contentType(MediaType.APPLICATION_JSON)
                .content(asJsonString(subcategoryOne))
        )
            .andDo(print())
            .andExpect(status().isOk())
    }

    @Test
    fun testUpdateSubCategory() {
        val updatedSubCategory = SubCategories(1, "UpdatedSubcategoryOne")
        `when`(subcategoryServiceImpl.updateSubCategory(1, updatedSubCategory)).thenReturn(updatedSubCategory)

        mockMvc.perform(
            put("/catalog/categories/subcategories/1")
                .contentType(MediaType.APPLICATION_JSON)
                .content(asJsonString(updatedSubCategory))
        )
            .andDo(print())
            .andExpect(status().isOk())
    }

    @Test
    fun testUpdateSubCategoryNotFound() {
        val updatedSubCategory = SubCategories(99, "UpdatedSubcategory")
        `when`(subcategoryServiceImpl.updateSubCategory(99, updatedSubCategory)).thenReturn(null)

        mockMvc.perform(
            put("/catalog/categories/subcategories/99")
                .contentType(MediaType.APPLICATION_JSON)
                .content(asJsonString(updatedSubCategory))
        )
            .andDo(print())
            .andExpect(status().isNotFound())
    }

    @Test
    fun testDeleteSubCategory() {
        mockMvc.perform(delete("/catalog/categories/subcategories/1"))
            .andDo(print())
            .andExpect(status().isNoContent())

        verify(subcategoryServiceImpl, times(1)).deleteSubCategory(1)
    }

    private fun asJsonString(obj: Any): String {
        return try {
            val objectMapper = ObjectMapper()
            objectMapper.writeValueAsString(obj)
        } catch (e: Exception) {
            throw RuntimeException(e)
        }
    }
}

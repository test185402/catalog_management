package com.springdemo.catalog_management.Controller

import com.fasterxml.jackson.databind.ObjectMapper
import com.springdemo.catalog_management.Controller.VariantController
import com.springdemo.catalog_management.Model.Categories
import com.springdemo.catalog_management.Model.Product
import com.springdemo.catalog_management.Model.SubCategories
import com.springdemo.catalog_management.Model.Variant
import com.springdemo.catalog_management.Service.VariantServiceImpl
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.Mockito.*
import org.mockito.junit.jupiter.MockitoExtension
import org.springframework.http.MediaType
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*
import org.springframework.test.web.servlet.result.MockMvcResultHandlers.print
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.status
import org.springframework.test.web.servlet.setup.MockMvcBuilders
import org.junit.jupiter.api.extension.ExtendWith

@ExtendWith(MockitoExtension::class)
class VariantControllerTest {

    private lateinit var mockMvc: MockMvc

    @Mock
    private lateinit var variantServiceImpl: VariantServiceImpl

    @InjectMocks
    private lateinit var variantController: VariantController

    private lateinit var variantOne: Variant
    private lateinit var variantTwo: Variant
    private lateinit var product: Product
    private lateinit var subcategory: SubCategories
    private lateinit var category: Categories

    @BeforeEach
    fun setUp() {
        mockMvc = MockMvcBuilders.standaloneSetup(variantController).build()

        category = Categories(1, "Electronics", "Electronic devices")
        subcategory = SubCategories(1, "SubcategoryOne",category)
        product = Product(0, "ProductOne", "DescriptionOne", 4.5, 3, "BrandOne", "ModelOne", 100,subcategory)
        variantOne = Variant(1, "Red", 100.0, 10.0, 0.5, "FeatureOne", "10x5x3", product)
    }

    @Test
    fun testGetAllVariants() {
        val variantsList = listOf(variantOne)
        `when`(variantServiceImpl.getVariants()).thenReturn(variantsList)

        mockMvc.perform(get("/catalog/categories/subcategories/products/variants"))
            .andDo(print())
            .andExpect(status().isOk())
    }

    @Test
    fun testGetVariantById() {
        val variantId = 1
        `when`(variantServiceImpl.getVariantById(variantId)).thenReturn(variantOne)

        mockMvc.perform(get("/catalog/categories/subcategories/products/variants/$variantId"))
            .andDo(print())
            .andExpect(status().isOk())
    }

    @Test
    fun testGetVariantByIdNotFound() {
        val variantId = 99
        `when`(variantServiceImpl.getVariantById(variantId)).thenReturn(null)

        mockMvc.perform(get("/catalog/categories/subcategories/products/variants/$variantId"))
            .andDo(print())
            .andExpect(status().isNotFound())
    }

    @Test
    fun testCreateVariant() {
        `when`(variantServiceImpl.createVariant(variantOne)).thenReturn(variantOne)

        mockMvc.perform(
            post("/catalog/categories/subcategories/products/variants")
                .contentType(MediaType.APPLICATION_JSON)
                .content(asJsonString(variantOne))
        )
            .andDo(print())
            .andExpect(status().isOk())
    }

    @Test
    fun testUpdateVariant() {
        val updatedVariant =  Variant(1, "Red updated", 100.0, 10.0, 0.5, "FeatureOne", "10x5x3", product)
        `when`(variantServiceImpl.updateVariant(1, updatedVariant)).thenReturn(updatedVariant)

        mockMvc.perform(
            put("/catalog/categories/subcategories/products/variants/1")
                .contentType(MediaType.APPLICATION_JSON)
                .content(asJsonString(updatedVariant))
        )
            .andDo(print())
            .andExpect(status().isOk())
    }

    @Test
    fun testUpdateVariantNotFound() {
        val updatedVariant = Variant(99, "Red updated", 100.0, 10.0, 0.5, "FeatureOne", "10x5x3", product)
        `when`(variantServiceImpl.updateVariant(99, updatedVariant)).thenReturn(null)

        mockMvc.perform(
            put("/catalog/categories/subcategories/products/variants/99")
                .contentType(MediaType.APPLICATION_JSON)
                .content(asJsonString(updatedVariant))
        )
            .andDo(print())
            .andExpect(status().isNotFound())
    }

    @Test
    fun testDeleteVariant() {
        mockMvc.perform(delete("/catalog/categories/subcategories/products/variants/1"))
            .andDo(print())
            .andExpect(status().isNoContent())

        verify(variantServiceImpl, times(1)).deleteVariant(1)
    }

    private fun asJsonString(obj: Any): String {
        return try {
            val objectMapper = ObjectMapper()
            objectMapper.writeValueAsString(obj)
        } catch (e: Exception) {
            throw RuntimeException(e)
        }
    }
}

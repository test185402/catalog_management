package com.springdemo.catalog_management.Controller
import com.fasterxml.jackson.databind.ObjectMapper
import org.junit.jupiter.api.Test
import com.springdemo.catalog_management.Model.Categories
import com.springdemo.catalog_management.Service.CategoryServiceImpl
import org.junit.jupiter.api.BeforeEach
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.Mockito.*
import org.mockito.junit.jupiter.MockitoExtension
import org.springframework.http.MediaType
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*
import org.springframework.test.web.servlet.result.MockMvcResultHandlers.print
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.status
import org.springframework.test.web.servlet.setup.MockMvcBuilders
import org.junit.jupiter.api.extension.ExtendWith

@ExtendWith(MockitoExtension::class)
class CategoryControllerTest {

        private lateinit var mockMvc: MockMvc

        @Mock
        private lateinit var categoryServiceImpl: CategoryServiceImpl

        @InjectMocks
        private lateinit var categoryController: CategoryController

        private lateinit var categoryOne: Categories
        private lateinit var categoryTwo: Categories

        @BeforeEach
        fun setUp() {
            mockMvc = MockMvcBuilders.standaloneSetup(categoryController).build()

            categoryOne = Categories(1, "Electronics", "Electronic devices")
            categoryTwo = Categories(2, "Clothing", "Fashion items")
        }

//    This test mocks the service to return a list of categories and then performs a GET request to "/catalog/categories" using MockMvc.
//    It checks that the HTTP status is OK (200).
        @Test
        fun testGetAllCategories() {
            val categoriesList = listOf(categoryOne, categoryTwo)
            `when`(categoryServiceImpl.getAllCategories()).thenReturn(categoriesList)

            mockMvc.perform(get("/catalog/categories"))
                .andDo(print())
                .andExpect(status().isOk())
        }

        @Test
        fun testGetCategoryById() {
            val categoryId = 1
            `when`(categoryServiceImpl.getCategoryById(categoryId)).thenReturn(categoryOne)

            mockMvc.perform(get("/catalog/categories/$categoryId"))
                .andDo(print())
                .andExpect(status().isOk())
        }

        @Test
        fun testGetCategoryByIdNotFound() {
            val categoryId = 99
            `when`(categoryServiceImpl.getCategoryById(categoryId)).thenReturn(null)

            mockMvc.perform(get("/catalog/categories/$categoryId"))
                .andDo(print())
                .andExpect(status().isNotFound())
        }

        @Test
        fun testCreateCategory() {
            `when`(categoryServiceImpl.createCategory(categoryOne)).thenReturn(categoryOne)

            mockMvc.perform(
                post("/catalog/categories")
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(asJsonString(categoryOne))
            )
                .andDo(print())
                .andExpect(status().isOk())
        }

        @Test
        fun testUpdateCategory() {
            val updatedCategory = Categories(1, "Updated Electronics", "Updated Electronic devices")
            `when`(categoryServiceImpl.updateCategory(1, updatedCategory)).thenReturn(updatedCategory)

            mockMvc.perform(
                put("/catalog/categories/1")
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(asJsonString(updatedCategory))
            )
                .andDo(print())
                .andExpect(status().isOk())
        }

        @Test
        fun testUpdateCategoryNotFound() {
            val updatedCategory = Categories(99, "Updated Category", "Updated Description")
            `when`(categoryServiceImpl.updateCategory(99, updatedCategory)).thenReturn(null)

            mockMvc.perform(
                put("/catalog/categories/99")
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(asJsonString(updatedCategory))
            )
                .andDo(print())
                .andExpect(status().isNotFound())
        }

        @Test
        fun testDeleteCategory() {
            mockMvc.perform(delete("/catalog/categories/1"))
                .andDo(print())
                .andExpect(status().isNoContent())

            verify(categoryServiceImpl, times(1)).deleteCategory(1)
        }

        private fun asJsonString(obj: Any): String {
            return try {
                val objectMapper = ObjectMapper()
                objectMapper.writeValueAsString(obj)
            } catch (e: Exception) {
                throw RuntimeException(e)
            }
        }
}
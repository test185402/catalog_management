package com.springdemo.catalog_management.Controller

import com.fasterxml.jackson.databind.ObjectMapper
import com.springdemo.catalog_management.Model.Categories
import com.springdemo.catalog_management.Model.Product
import com.springdemo.catalog_management.Model.SubCategories
import com.springdemo.catalog_management.Service.ProductServiceImple
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.Mockito.*
import org.mockito.junit.jupiter.MockitoExtension
import org.springframework.http.MediaType
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*
import org.springframework.test.web.servlet.result.MockMvcResultHandlers.print
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.status
import org.springframework.test.web.servlet.setup.MockMvcBuilders
import org.junit.jupiter.api.extension.ExtendWith

@ExtendWith(MockitoExtension::class)
class ProductControllerTest {

    private lateinit var mockMvc: MockMvc

    @Mock
    private lateinit var productServiceImple: ProductServiceImple

    @InjectMocks
    private lateinit var productController: ProductController
    private lateinit var subcategoryOne: SubCategories
    private lateinit var categoryOne: Categories
    private lateinit var productOne: Product
    private lateinit var productTwo: Product

    @BeforeEach
    fun setUp() {
        mockMvc = MockMvcBuilders.standaloneSetup(productController).build()
        categoryOne = Categories(1, "Electronics", "Electronic devices")
        subcategoryOne = SubCategories(1, "SubcategoryOne",categoryOne)

        productOne = Product(0, "ProductOne", "DescriptionOne", 4.5, 3, "BrandOne", "ModelOne", 100,subcategoryOne)
        productTwo = Product(2, "ProductTwo", "DescriptionTwo", 3.8, 2, "BrandTwo", "ModelTwo", 75,subcategoryOne)
    }

    @Test
    fun testGetAllProducts() {
        val productsList = listOf(productOne, productTwo)
        `when`(productServiceImple.getAllProducts()).thenReturn(productsList)

        mockMvc.perform(get("/catalog/categories/subcategories/products"))
            .andDo(print())
            .andExpect(status().isOk())
    }

    @Test
    fun testGetProductById() {
        val productId = 1
        `when`(productServiceImple.getProductById(productId)).thenReturn(productOne)

        mockMvc.perform(get("/catalog/categories/subcategories/products/$productId"))
            .andDo(print())
            .andExpect(status().isOk())
    }

    @Test
    fun testGetProductByIdNotFound() {
        val productId = 99
        `when`(productServiceImple.getProductById(productId)).thenReturn(null)

        mockMvc.perform(get("/catalog/categories/subcategories/products/$productId"))
            .andDo(print())
            .andExpect(status().isNotFound())
    }

    @Test
    fun testCreateProduct() {
        `when`(productServiceImple.createProduct(productOne)).thenReturn(productOne)

        mockMvc.perform(
            post("/catalog/categories/subcategories/products")
                .contentType(MediaType.APPLICATION_JSON)
                .content(asJsonString(productOne))
        )
            .andDo(print())
            .andExpect(status().isOk())
    }

    @Test
    fun testUpdateProduct() {
        val updatedProduct = Product(0, "UpdatedProductOne", "UpdatedDescriptionOne", 4.8, 2, "UpdatedBrandOne", "UpdatedModelOne", 120)
        `when`(productServiceImple.updateProduct(1, updatedProduct)).thenReturn(updatedProduct)

        mockMvc.perform(
            put("/catalog/categories/subcategories/products/1")
                .contentType(MediaType.APPLICATION_JSON)
                .content(asJsonString(updatedProduct))
        )
            .andDo(print())
            .andExpect(status().isOk())
    }

    @Test
    fun testUpdateProductNotFound() {
        val updatedProduct = Product(0, "UpdatedProduct", "UpdatedDescription", 4.2, 1, "UpdatedBrand", "UpdatedModel", 90)
        `when`(productServiceImple.updateProduct(99, updatedProduct)).thenReturn(null)

        mockMvc.perform(
            put("/catalog/categories/subcategories/products/99")
                .contentType(MediaType.APPLICATION_JSON)
                .content(asJsonString(updatedProduct))
        )
            .andDo(print())
            .andExpect(status().isNotFound())
    }

    @Test
    fun testDeleteProduct() {
        mockMvc.perform(delete("/catalog/categories/subcategories/products/1"))
            .andDo(print())
            .andExpect(status().isNoContent())

        verify(productServiceImple, times(1)).deleteProduct(1)
    }

    private fun asJsonString(obj: Any): String {
        return try {
            val objectMapper = ObjectMapper()
            objectMapper.writeValueAsString(obj)
        } catch (e: Exception) {
            throw RuntimeException(e)
        }
    }
}


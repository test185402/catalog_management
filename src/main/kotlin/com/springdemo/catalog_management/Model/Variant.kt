package com.springdemo.catalog_management.Model

import jakarta.persistence.*
import java.awt.Color

//Variant
//**Product_id
//*Variant_id
//price
//color
//Description
//dimensions
//Weight


@Entity
@Table(name="variant")
data class Variant(
    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    val variantId :Int?,
    val color: String,
    val mrp: Double,
    val discount:Double,
    val weight:Double,
    val features:String,
    val dimensions:String,

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name="ProductId", referencedColumnName = "ProductId")

    var product: Product?= null){
    constructor(): this(null,"",0.0,0.0,0.0,"","")
}
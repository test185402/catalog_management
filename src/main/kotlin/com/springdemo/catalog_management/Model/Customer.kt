package com.springdemo.catalog_management.Model

import jakarta.persistence.*

@Entity
@Table(name="customer")
data class Customer(
    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    val customerId :Int? =0,
    val firstName: String,
    val lastName: String,
    val email: String,
    val address: String,
    val phoneNumber: Int?
){
    constructor(): this(null,"","","","",0)
}
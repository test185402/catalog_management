package com.springdemo.catalog_management.Model

import jakarta.persistence.*
import javax.management.Descriptor

//Product
//*id
//Name
//**Sub_category_id
//description
//rating
//Variant_count
//Brand

@Entity
@Table(name="Product")
data class Product (
@Id @GeneratedValue(strategy = GenerationType.IDENTITY)
val ProductId :Int? =0,
val productName: String,
val description: String,
val rating: Double,
val variantCount: Int?= 0,
val brand:String,
val model:String,
val cost:Int,

@ManyToOne(fetch = FetchType.EAGER,cascade = [CascadeType.PERSIST, CascadeType.MERGE])
@JoinColumn(name="subCategoriesId", referencedColumnName = "subCategoriesId")
val subCategories: SubCategories?= null){
    constructor(): this(null,"","",0.0,null,"","",0)
}
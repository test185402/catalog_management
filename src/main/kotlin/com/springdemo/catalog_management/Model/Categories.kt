package com.springdemo.catalog_management.Model

import jakarta.persistence.*
import org.springframework.context.annotation.Primary

@Entity
@Table(name="categories")
data class Categories(
    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    val categoryId :Int?=null,
    val name : String="",
    val description: String="",
){
    constructor():this(null,"","")
}

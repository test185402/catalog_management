package com.springdemo.catalog_management.Model

import jakarta.persistence.*

//Sub-categories
//*Sub_categories_id
//**Category_id



@Entity
@Table(name="subCategories")
data class SubCategories(
    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)

    val subCategoriesId :Int?=null,
    val categoryName: String="",

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name="categoryId", referencedColumnName = "categoryId")
    val categories: Categories?=null){
    constructor(): this(null,"",null)
}


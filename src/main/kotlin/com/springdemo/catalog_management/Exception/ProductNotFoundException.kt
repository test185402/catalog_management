package com.springdemo.catalog_management.Exception
class ProductNotFoundException(message: String) : RuntimeException(message)
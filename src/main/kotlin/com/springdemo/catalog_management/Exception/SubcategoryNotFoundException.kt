package com.springdemo.catalog_management.Exception

class SubcategoryNotFoundException(message: String) : RuntimeException(message)
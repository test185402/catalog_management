package com.springdemo.catalog_management.Exception

class VariantNotFoundException(message: String) : RuntimeException(message)
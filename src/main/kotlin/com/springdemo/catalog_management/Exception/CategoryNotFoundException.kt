package com.springdemo.catalog_management.Exception
class CategoryNotFoundException(message: String) : RuntimeException(message)
package com.springdemo.catalog_management.graphql

import com.springdemo.catalog_management.Model.Categories
import com.springdemo.catalog_management.Service.CategoryService
import graphql.kickstart.tools.GraphQLMutationResolver
import graphql.kickstart.tools.GraphQLQueryResolver
import org.springframework.context.annotation.Bean
import org.springframework.stereotype.Component


@Component
class CategoryResolver (private val categoryService: CategoryService) : GraphQLQueryResolver, GraphQLMutationResolver {

    fun getAllCategories(): List<Categories> = categoryService.getAllCategories()

    fun getCategoryById(id: Int): Categories? = categoryService.getCategoryById(id)

    fun createCategory(name: String, description: String): Categories =
        categoryService.createCategory(Categories(name = name, description = description))

    fun updateCategory(id: Int, name: String, description: String): Categories? =
        categoryService.updateCategory(id, Categories(name = name, description = description))

    fun deleteCategory(id: Int): Boolean {
        categoryService.deleteCategory(id)
        return true
    }
}


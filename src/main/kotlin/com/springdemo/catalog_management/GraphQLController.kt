package com.springdemo.catalog_management

import graphql.ExecutionResult
import graphql.GraphQL
import graphql.kickstart.tools.SchemaParser
import graphql.schema.GraphQLSchema
import org.springframework.beans.factory.annotation.Value
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import org.springframework.core.io.Resource
import com.springdemo.catalog_management.graphql.CategoryResolver
import org.springframework.context.annotation.ComponentScan

@ComponentScan(basePackages = ["com.springdemo.catalog_management.graphql"])
@RestController
@RequestMapping("/graphql")
class GraphQLController(private val categoryResolver: CategoryResolver) {

    @Value("classpath:graphql/schema.graphqls")
    private lateinit var schemaFilePath: Resource

    @PostMapping
    fun executeQuery(@RequestBody query: String): ResponseEntity<Any> {
        val graphQL: GraphQL = createGraphQLInstance()
        val executionResult: ExecutionResult = graphQL.execute(query)
        return ResponseEntity.ok(executionResult.getData())
    }

    private fun createGraphQLInstance(): GraphQL {
        val schema: GraphQLSchema = SchemaParser.newParser()
            .file("graphql/schema.graphqls")
            .resolvers(categoryResolver)
            .build()
            .makeExecutableSchema()

        return GraphQL.newGraphQL(schema).build()
    }
}




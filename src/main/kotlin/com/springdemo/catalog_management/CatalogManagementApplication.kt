package com.springdemo.catalog_management

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.context.annotation.ComponentScan

@SpringBootApplication
class CatalogManagementApplication

fun main(args: Array<String>) {
    runApplication<CatalogManagementApplication>(*args)
}



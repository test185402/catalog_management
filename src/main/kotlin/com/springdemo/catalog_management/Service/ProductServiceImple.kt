package com.springdemo.catalog_management.Service

import com.springdemo.catalog_management.Exception.CategoryNotFoundException
import com.springdemo.catalog_management.Exception.ProductNotFoundException
import com.springdemo.catalog_management.Model.Categories
import com.springdemo.catalog_management.Model.Product
import com.springdemo.catalog_management.Repository.CategoriesRepository
import com.springdemo.catalog_management.Repository.ProductRepository
import org.springframework.stereotype.Service

interface ProductService {
    fun getAllProducts(): List<Product>
    fun getProductById(id: Int): Product?
    fun createProduct(product: Product): Product
    fun updateProduct(id: Int, updatedProduct: Product): Product?
    fun deleteProduct(id: Int)
}

@Service
class ProductServiceImple  (private val productRepository: ProductRepository) : ProductService{

    override fun getAllProducts(): List<Product> {
        return productRepository.findAll()
    }

    override fun getProductById(id: Int): Product? {
        return productRepository.findById(id).orElse(null)
    }

    override fun createProduct(product: Product): Product {
        return productRepository.save(product)
    }

    override fun updateProduct(id: Int, updatedProduct: Product): Product? {
        return productRepository.findById(id).map { existingProduct ->
            val updated = existingProduct.copy(productName = updatedProduct.productName, description = updatedProduct.description,rating=updatedProduct.rating,
                variantCount=updatedProduct.variantCount,brand=updatedProduct.brand,model=updatedProduct.model,cost=updatedProduct.cost)
            productRepository.save(updated)
        }.orElse(null)
    }

    override fun deleteProduct(id: Int) {
//        productRepository.findById(id).ifPresent { entity ->
//            productRepository.delete(entity)}
        if (productRepository.existsById(id)) {
            productRepository.deleteById(id)
        } else {
            throw ProductNotFoundException("Product with id $id not found")
        }
    }

}

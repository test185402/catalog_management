package com.springdemo.catalog_management.Service
import com.springdemo.catalog_management.Exception.SubcategoryNotFoundException
import com.springdemo.catalog_management.Model.SubCategories
import com.springdemo.catalog_management.Repository.SubCategoriesRepository
import org.springframework.stereotype.Service

interface SubCategoryService {
    fun getAllSubCategories(): List<SubCategories>
    fun getSubCategoryById(id: Int): SubCategories?
    fun createSubCategory(subCategories: SubCategories): SubCategories
    fun updateSubCategory(id: Int, updatedSubCategory: SubCategories): SubCategories?
    fun deleteSubCategory(id: Int)
}


@Service
class SubcategoryServiceImpl (private val subCategoriesRepository: SubCategoriesRepository) : SubCategoryService{

    override fun getAllSubCategories(): List<SubCategories> {
        return subCategoriesRepository.findAll()
    }

    override fun getSubCategoryById(id: Int): SubCategories? {
        return subCategoriesRepository.findById(id).orElse(null)
    }

    override fun createSubCategory(subCategories: SubCategories): SubCategories {
        return subCategoriesRepository.save(subCategories)
    }

    override fun updateSubCategory(id: Int, updatedSubCategory: SubCategories): SubCategories? {
        return subCategoriesRepository.findById(id).map { existingCategory ->
            val updated = existingCategory.copy(categoryName = updatedSubCategory.categoryName)
            subCategoriesRepository.save(updated)
        }.orElse(null)
    }

    override fun deleteSubCategory(id: Int) {
//        subCategoriesRepository.findById(id).ifPresent { entity ->
//            subCategoriesRepository.delete(entity)
        if (subCategoriesRepository.existsById(id)) {
            subCategoriesRepository.deleteById(id)
        } else {
            throw SubcategoryNotFoundException("Subcategory with ID $id not found")
        }
    }
}

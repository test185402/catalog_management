package com.springdemo.catalog_management.Service
import com.springdemo.catalog_management.Exception.ProductNotFoundException
import com.springdemo.catalog_management.Exception.VariantNotFoundException
import com.springdemo.catalog_management.Model.Variant
import com.springdemo.catalog_management.Repository.ProductRepository
import com.springdemo.catalog_management.Repository.VariantRepository
import org.springframework.stereotype.Service

interface VariantService {
    fun getVariants(): List<Variant>
    fun getVariantById(id: Int): Variant?
    fun createVariant(variant: Variant): Variant
    fun updateVariant(id: Int, updatedVariant: Variant): Variant?
    fun deleteVariant(id: Int)
}

@Service
class VariantServiceImpl(
    private val variantRepository: VariantRepository,
    private val productService: ProductService,
    private val productRepository: ProductRepository
) : VariantService{

    override fun getVariants(): List<Variant> {
        return variantRepository.findAll()
    }

    override fun getVariantById(id: Int): Variant? {
        return variantRepository.findById(id).orElse(null)
    }

    override fun createVariant(variant: Variant): Variant {
        var product = variant.product
        if (product != null) {
            if (product.ProductId == null) {
                val savedProduct = productService.createProduct(product)
                variant.product = savedProduct
            }
        }
        return variantRepository.save(variant)
    }


    override fun updateVariant(id: Int, updatedVariant: Variant): Variant? {
        return variantRepository.findById(id).map { existingVariant ->
            val updated = existingVariant.copy(color = updatedVariant.color, mrp = updatedVariant.mrp,discount=updatedVariant.discount,
                weight=updatedVariant.weight,features=updatedVariant.features,dimensions=updatedVariant.dimensions)
            variantRepository.save(updated)
        }.orElse(null)
    }

    override fun deleteVariant(id: Int) {
//        variantRepository.findById(id).ifPresent { variant ->
//            variantRepository.delete(variant)
//        }
        if (variantRepository.existsById(id)) {
            variantRepository.deleteById(id)
        } else {
            throw VariantNotFoundException("Variant with id $id not found")
        }
    }

}


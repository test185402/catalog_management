package com.springdemo.catalog_management.Service

import com.springdemo.catalog_management.Exception.CategoryNotFoundException
import com.springdemo.catalog_management.Model.Categories
import com.springdemo.catalog_management.Repository.CategoriesRepository
import org.springframework.stereotype.Service

interface CategoryService {
    fun getAllCategories(): List<Categories>
    fun getCategoryById(id: Int): Categories?
    fun createCategory(categories: Categories): Categories
    fun updateCategory(id: Int, updatedCategory: Categories): Categories?
    fun deleteCategory(id: Int)
}


@Service
class CategoryServiceImpl (private val categoriesRepository: CategoriesRepository) : CategoryService{

    override fun getAllCategories(): List<Categories> {
        return categoriesRepository.findAll()
    }

    override fun getCategoryById(id: Int): Categories? {
        return categoriesRepository.findById(id).orElse(null)
    }

    override fun createCategory(categories: Categories): Categories {
        return categoriesRepository.save(categories)
    }

    override fun updateCategory(id: Int, updatedCategory: Categories): Categories? {
        return categoriesRepository.findById(id).map { existingCategory ->
            val updated = existingCategory.copy(name = updatedCategory.name, description = updatedCategory.description)
            categoriesRepository.save(updated)
        }.orElse(null)
    }

    override fun deleteCategory(id: Int) {
//        categoriesRepository.findById(id).ifPresent { entity ->
//            categoriesRepository.delete(entity)
//        }
        if (categoriesRepository.existsById(id)) {
            categoriesRepository.deleteById(id)
        } else {
            throw CategoryNotFoundException("Category with id $id not found")
        }
    }
}
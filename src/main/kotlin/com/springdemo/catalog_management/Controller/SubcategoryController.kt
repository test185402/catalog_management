package com.springdemo.catalog_management.Controller

import com.springdemo.catalog_management.Model.SubCategories
import com.springdemo.catalog_management.Service.SubcategoryServiceImpl
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("/catalog/categories")
class SubcategoryController(private val subcategoryServiceImpl: SubcategoryServiceImpl){

    @GetMapping("/subcategories")
    fun getAllSubCategories(): List<SubCategories>{
        return subcategoryServiceImpl.getAllSubCategories()
    }

    @GetMapping("/subcategories/{id}")
    fun getSubCategoryById(@PathVariable id: Int): ResponseEntity<SubCategories> {
        val subCategory = subcategoryServiceImpl.getSubCategoryById(id)
        return if (subCategory != null) {
            ResponseEntity.ok(subCategory)
        } else {
            ResponseEntity.notFound().build()
        }
    }

    @PostMapping("/subcategories")
    fun createSubCategory(@RequestBody subCategories: SubCategories):  SubCategories {
        return subcategoryServiceImpl.createSubCategory(subCategories)
    }



    @PutMapping("/subcategories/{id}")
    fun updateSubCategory(@PathVariable id: Int, @RequestBody updatedSubCategory: SubCategories): ResponseEntity<SubCategories> {
        val updated = subcategoryServiceImpl.updateSubCategory(id, updatedSubCategory)
        return if (updated != null) {
            ResponseEntity.ok(updated)
        } else {
            ResponseEntity.notFound().build()
        }
    }

    @DeleteMapping("/subcategories/{id}")
    fun deleteSubCategory(@PathVariable id: Int): ResponseEntity<Void> {
        subcategoryServiceImpl.deleteSubCategory(id)
        return ResponseEntity.noContent().build()
    }

}
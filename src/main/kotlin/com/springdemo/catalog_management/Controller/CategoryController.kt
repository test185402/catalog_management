package com.springdemo.catalog_management.Controller
import com.springdemo.catalog_management.Model.Categories
import com.springdemo.catalog_management.Repository.CategoriesRepository
import com.springdemo.catalog_management.Service.CategoryServiceImpl
import org.springframework.beans.factory.annotation.Value
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("/catalog")
class CategoryController(private val categoryServiceImpl: CategoryServiceImpl) {

    @GetMapping("/categories")
    fun getAllCategories(): List<Categories> {
        return categoryServiceImpl.getAllCategories()
    }

    @GetMapping("/categories/{id}")
    fun getCategoryById(@PathVariable id: Int): ResponseEntity<Categories> {
        val category = categoryServiceImpl.getCategoryById(id)
        return if (category != null) {
            ResponseEntity.ok(category)
        } else {
            ResponseEntity.notFound().build()
        }
    }

    @PostMapping("/categories")
    fun createCategory(@RequestBody categories: Categories): Categories {
        return categoryServiceImpl.createCategory(categories)
    }

    @PutMapping("/categories/{id}")
    fun updateCategory(@PathVariable id: Int, @RequestBody updatedCategory: Categories): ResponseEntity<Categories> {
        val updated = categoryServiceImpl.updateCategory(id, updatedCategory)
        return if (updated != null) {
            ResponseEntity.ok(updated)
        } else {
            ResponseEntity.notFound().build()
        }
    }

    @DeleteMapping("/categories/{id}")
    fun deleteCategory(@PathVariable id: Int): ResponseEntity<Void> {
        categoryServiceImpl.deleteCategory(id)
        return ResponseEntity.noContent().build()
    }


}
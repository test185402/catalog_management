package com.springdemo.catalog_management.Controller

import com.springdemo.catalog_management.Model.Product
import com.springdemo.catalog_management.Service.ProductServiceImple
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("/catalog/categories/subcategories")
class ProductController(private val productServiceImple: ProductServiceImple){


    @GetMapping("/products")
    fun getAllProducts(): List<Product>{
        return productServiceImple.getAllProducts()
    }

    @GetMapping("/products/{id}")
    fun getProductById(@PathVariable id: Int): ResponseEntity<Product> {
        val product = productServiceImple.getProductById(id)
        return if (product != null) {
            ResponseEntity.ok(product)
        } else {
            ResponseEntity.notFound().build()
        }
    }

    @PostMapping("/products")
    fun createCategory(@RequestBody products: Product): Product {
        return productServiceImple.createProduct(products)
    }

    @PutMapping("/products/{id}")
    fun updateCategory(@PathVariable id: Int, @RequestBody updatedProduct: Product): ResponseEntity<Product> {
        val updated = productServiceImple.updateProduct(id, updatedProduct)
        return if (updated != null) {
            ResponseEntity.ok(updated)
        } else {
            ResponseEntity.notFound().build()
        }
    }

    @DeleteMapping("/products/{id}")
    fun deleteCategory(@PathVariable id: Int): ResponseEntity<Void> {
        productServiceImple.deleteProduct(id)
        return ResponseEntity.noContent().build()
    }


}
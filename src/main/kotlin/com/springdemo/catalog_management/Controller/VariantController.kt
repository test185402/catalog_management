package com.springdemo.catalog_management.Controller

import com.springdemo.catalog_management.Model.Variant
import com.springdemo.catalog_management.Service.VariantServiceImpl
import com.springdemo.catalog_management.Model.Product
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("/catalog/categories/subcategories/products")
class VariantController(private val variantServiceImpl: VariantServiceImpl){


    @GetMapping("/variants")
    fun getAllVariants(): List<Variant>{
        return variantServiceImpl.getVariants()
    }

    @GetMapping("/variants/{id}")
    fun getVariantById(@PathVariable id: Int): ResponseEntity<Variant> {
        val variant = variantServiceImpl.getVariantById(id)
        return if (variant != null) {
            ResponseEntity.ok(variant)
        } else {
            ResponseEntity.notFound().build()
        }
    }

    @PostMapping("/variants")
    fun createVariant(@RequestBody variant: Variant): Variant {
        return variantServiceImpl.createVariant(variant)
    }

    @PutMapping("/variants/{id}")
    fun updateVariant(@PathVariable id: Int, @RequestBody updatedVariant: Variant): ResponseEntity<Variant> {
        val updated = variantServiceImpl.updateVariant(id, updatedVariant)
        return if (updated != null) {
            ResponseEntity.ok(updated)
        } else {
            ResponseEntity.notFound().build()
        }
    }

    @DeleteMapping("/variants/{id}")
    fun deleteVariant(@PathVariable id: Int): ResponseEntity<Void> {
        variantServiceImpl.deleteVariant(id)
        return ResponseEntity.noContent().build()
    }


}
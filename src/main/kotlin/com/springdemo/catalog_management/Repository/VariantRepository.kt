package com.springdemo.catalog_management.Repository
import com.springdemo.catalog_management.Model.Categories
import com.springdemo.catalog_management.Model.Variant
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

@Repository
public interface VariantRepository: JpaRepository<Variant, Int>
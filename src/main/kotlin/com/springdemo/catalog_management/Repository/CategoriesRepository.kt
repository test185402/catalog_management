package com.springdemo.catalog_management.Repository

import com.springdemo.catalog_management.Model.Categories
import com.springdemo.catalog_management.Model.SubCategories
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

@Repository

public interface CategoriesRepository: JpaRepository<Categories, Int> {
}